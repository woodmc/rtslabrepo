﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RTSLabs
{
    class Program
    {
        static void Main(string[] args)
        {
            // Running AboveBelow Program
            int[] input = new int[] { 1, 5, 2, 1, 10 };
            int compareValue = 2;

            Dictionary<string, int> results = AboveBelow(input, compareValue);

            foreach (KeyValuePair<string, int> listItem in results)
            {
                Console.WriteLine("{0} : {1}", listItem.Key, listItem.Value);
            }


            // Running String Rotation Program
            string stringInput = "MyString";
            int rotationAmount = 2;

            Console.WriteLine(StringRotation(stringInput, rotationAmount));


            Console.ReadLine();
        }

        private static Dictionary<string, int> AboveBelow(int[] numberList, int compareValue)
        {
            Dictionary<string, int> resultList = new Dictionary<string, int>
            {
                { "above", numberList.Where(num => num > compareValue).Count() },
                { "below", numberList.Where(num => num < compareValue).Count() }
            };

            return resultList;
        }

        private static string StringRotation(string input, int rotationAmount)
        {
            string rotatedString = input;

            if (!string.IsNullOrEmpty(input) && rotationAmount <= input.Length)
            {
                rotatedString = input[(input.Length - rotationAmount)..] + input[..(input.Length - rotationAmount)];
            }

            return rotatedString;
        }
    }
}
